FROM ubuntu:trusty

ENV DEBIAN_FRONTEND noninteractive

# do not run services immediately
RUN echo -e "#!/bin/sh\nexit 101\n" > /usr/sbin/policy-rc.d && chmod +x /usr/sbin/policy-rc.d

RUN apt-get -q update
RUN apt-get -yq install apache2 libapache2-mod-php5 php5-mysql php5-sqlite php5-json php5-gd php5-xcache php5-xdebug php5-intl php5-curl

COPY sendmail.sh /usr/sbin/sendmail

COPY conf/apache.conf /etc/apache2/sites-available/000-default.conf

RUN a2enmod actions rewrite 2>&1 >/dev/null

ENV LOG_DIR /var/log/www
ENV UID 1000
ENV GID 1000

EXPOSE 80

COPY run.sh /run.sh

CMD ["/run.sh"]
