#!/bin/bash

if [ -z "$DOCUMENT_ROOT" ]
then
    echo "Did you forget to set the DOCUMENT_ROOT environment variable?" >&2
    exit 1
fi

mkdir -p "$LOG_DIR"

if [ ! -f /initialized ]
then 
    if ! grep $GID /etc/group; then addgroup --gid=$GID user; fi
    if ! grep $UID /etc/passwd; then adduser --uid=$UID --gid=$GID --shell=/bin/bash --gecos "" --disabled-password user; fi
    touch /initialized
fi

export APACHE_LOCK_DIR=/var/lock
export APACHE_PID_FILE=/var/run/apache.pid
export APACHE_RUN_USER="#$GID"
export APACHE_RUN_GROUP="#$UID"
export APACHE_LOG_DIR="$LOG_DIR"

apache2 -D FOREGROUND
