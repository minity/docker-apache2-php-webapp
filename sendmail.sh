#!/bin/bash
MAIL_LOG=${LOG_DIR:-/var/log}/mails.log
echo '== [' $(date +%Y-%m-%d_%H-%M-%S) '] ==' >> $MAIL_LOG
perl -MMIME::QuotedPrint -pe '$_=MIME::QuotedPrint::decode($_);' | tee -a $MAIL_LOG > /dev/null
